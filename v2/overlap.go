// Utility to calculate the overlap from a blast result

package blast

import(
    "fmt"
    "sort"
    "strings"
)


// Struct used to get the overlapping regions in the blast
type aln struct {
    Query   string
    Subject string
    Flipped int
    Qstart  int
    Qend    int
    Sstart  int
    Send    int
}

func (a aln) Eq(oa aln) bool {
    return a.Query == oa.Query && a.Subject == oa.Subject
}

type Match []aln

// Extracts matched regions from the blast results
// If alignment length is lower than min, aln is ignored
func (b Blast) GetMatchedRegions(min int) (Match, error) {
    if len(b.data) == 0 {
        return nil, fmt.Errorf("EmptyBlastError")
    }
    // Checking for missing required columns
    iq, qok := (*b.data[0].index)["qseqid"]
    is, sok := (*b.data[0].index)["sseqid"]
    iss, ssok := (*b.data[0].index)["sstart"]
    ise, seok := (*b.data[0].index)["send"]
    iqs, qsok := (*b.data[0].index)["qstart"]
    iqe, qeok := (*b.data[0].index)["qend"]
    if !qok || !sok || !ssok || !seok || !qsok || !qeok {
        var missing strings.Builder
        var sep = ""
        if !qok  { missing.WriteString(sep + "qseqid"); sep = ", " }
        if !sok  { missing.WriteString(sep + "sseqid"); sep = ", " }
        if !ssok { missing.WriteString(sep + "sstart"); sep = ", " }
        if !seok { missing.WriteString(sep + "ssend" ); sep = ", " }
        if !qsok { missing.WriteString(sep + "qstart"); sep = ", " }
        if !qeok { missing.WriteString(sep + "qend") }

        err := fmt.Errorf("Missing required column(s) in blast : %v", missing.String())
        return nil, err
    }
    // Getting match struct from table
    m := make(Match, len(b.data), len(b.data))
    for idx, row := range b.data{
        qu := row.data[iq].(string)
        su := row.data[is].(string)
        qs := row.data[iqs].(int)
        qe := row.data[iqe].(int)
        var flip int
        if qs > qe {
            qs, qe = qe, qs
            flip += 1
        }
        ss := row.data[iss].(int)
        se := row.data[ise].(int)
        if ss > se {
            ss, se = se, ss
            flip += 2
        }
        if (qe - qs) < min || (se - ss) < min { continue } // Ignore aln shorter than min

        m[idx] = aln{Query:  qu, Subject: su, Flipped: flip,
                     Qstart: qs, Qend: qe,
                     Sstart: ss, Send: se }
    }

    return m, nil
}

// Implementing the sort interface to sort our Match struct
type matchLessFunc func(p1, p2 *aln) bool

// multiSorter implements the Sort interface, sorting the changes within.
type matchSorter struct {
    match   Match
    less    []matchLessFunc
}

// Sort sorts the argument slice according to the less functions passed to OrderedBy.
func (ms *matchSorter) Sort(m Match) {
    ms.match = m
    sort.Sort(ms)
}

// OrderedBy returns a Sorter that sorts using the less functions, in order.
// Call its Sort method to sort the data.
func OrderMatchBy(less ...matchLessFunc) *matchSorter {
    return &matchSorter{
            less: less,
    }
}

// Len is part of sort.Interface.
func (ms *matchSorter) Len() int {
    return len(ms.match)
}

// Swap is part of sort.Interface.
func (ms *matchSorter) Swap(i, j int) {
    ms.match[i], ms.match[j] = ms.match[j], ms.match[i]
}

// Less is part of sort.Interface. It is implemented by looping along the
// less functions until it finds a comparison that discriminates between
// the two items (one is less than the other). 
func (ms *matchSorter) Less(i, j int) bool {
    p, q := &ms.match[i], &ms.match[j]
    // Try all but the last comparison.
    var k int
    for k = 0; k < len(ms.less)-1; k++ {
        less := ms.less[k]
        switch {
        case less(p, q):
            // p < q, so we have a decision.
            return true
        case less(q, p):
            // p > q, so we have a decision.
            return false
        }
        // p == q; try the next comparison.
    }
    // All comparisons to here said "equal", so just return whatever
    // the final comparison reports.
    return ms.less[k](p, q)
}
var Query = func(c1, c2 *aln) bool {
    return c1.Query < c2.Query
}
var Subject = func(c1, c2 *aln) bool {
    return c1.Subject < c2.Subject
}
var QueryStart = func(c1, c2 *aln) bool {
    return c1.Qstart < c2.Qstart
}
var QueryEnd = func(c1, c2 *aln) bool {
    return c1.Qend < c2.Qend
}
var SubjectStart = func(c1, c2 *aln) bool {
    return c1.Sstart < c2.Sstart
}
var SubjectEnd = func(c1, c2 *aln) bool {
    return c1.Send < c2.Send
}


type intv struct {
    Start  int
    End    int
}

type region struct{
    Range intv      // Begin-end of overlap
    Seqs  []string  // Sequences in overlap
}

type Overlap struct {
    Name    string
    Regions []region
    Gaps    []int     // Space between overlapping regions
}


// Map linking query and subject and pointing to Match struct
type OverlapMap map[string]map[string]Overlap

func (m OverlapMap) NewEntry(a aln, o Overlap, name string) {
    _, ok := m[a.Query]
    if !ok {
        m[a.Query] = make(map[string]Overlap)
    }
    _, ok = m[a.Query][a.Subject]
    if !ok {
        m[a.Query][a.Subject] = o
    }
}


func (m Match) GetOverlap(name string, gap int) (OverlapMap, OverlapMap) {
    // Il faut ajouter un map qui pointe sur un autre map qui lui pointera sur un overlap 
    // Donc un query pointe sur un subject et pointe sur son overlap propre
    // Si jamais on a un flag pour spécifier que l'ensmble des séquences proviennent d'un même organisme, 
    // On pourra ne pas se soucier de tout ça et concaténer l'ensemble des séquences?
    var soIns bool
    var qoIns bool
    mQuerySub := make(OverlapMap)
    mSubQuery := make(OverlapMap)
    if len(m) == 0 { return mQuerySub, mSubQuery }

    current := m[0]

    name = fmt.Sprintf("%v-%v", current.Query, current.Subject)
    qo := Overlap{Name: name}
    so := Overlap{Name: name}

    qi := intv{Start: current.Qstart, End: current.Qend}
    si := intv{Start: current.Sstart, End: current.Send}
    currentQi := region{Range: qi, Seqs: []string{current.Query}}
    currentSi := region{Range: si, Seqs: []string{current.Subject}}

    if len(m) == 1 {
        qo.Regions, so.Regions = []region{currentQi}, []region{currentSi}

    } else {

        for _, al := range m[1:] {
            // À chaque itération, on doit : 
            // 1. Comparer l'object courant au nouveau
            //      a. S'il est identique, on poursuit, sinon, 
            // 2. On enregistre l'object courant et on en commence un nouveau. 
            // 3. On créé un nouvel object overlap, et ainsi de suite
            //      b. L'object overlap ne sera créé que lors de l'ajout dans le map. 
            //         Cela permet de simplifier le processus et de la garder relativement comme il l'est déjà.
            //
            // Il faut donc :
            // 1. modifier/Corriger la méthode Eq pour l'object aln, 
            // 2. Ajouter une méthode pour aller chercher un object overlap s'il existe déjà
            // Checking alignment for query on subject
            if !current.Eq(al) {
                // Different item, we need to record the current item and start a new one

                // Recording
                if !soIns { so.Regions = append(so.Regions, currentSi) }
                if !qoIns { qo.Regions = append(qo.Regions, currentQi) }
                soIns = false
                qoIns = false

                mSubQuery.NewEntry(current, so, name)
                mQuerySub.NewEntry(current, qo, name)

                // Starting new item
                current = al
                name = fmt.Sprintf("%v-%v", current.Query, current.Subject)

                qo = Overlap{Name: name}
                so = Overlap{Name: name}

                si = intv{Start: current.Sstart, End:current.Send}
                currentSi = region{Range:si, Seqs: []string{current.Subject}}
                qi = intv{Start: current.Qstart, End:current.Qend}
                currentQi = region{Range:qi, Seqs: []string{current.Query}}
                continue

            }
            if al.Sstart <= currentSi.Range.End +1 +gap {
                soIns = false
                if currentSi.Range.End < al.Send {
                    currentSi.Range.End = al.Send
                }
                currentSi.Seqs = append(currentSi.Seqs, al.Subject)

            } else {
                soIns = true
                so.Regions = append(so.Regions, currentSi)
                so.Gaps = append(so.Gaps, al.Sstart - currentSi.Range.End)
                si = intv{Start: al.Sstart, End:al.Send}
                currentSi = region{Range:si, Seqs: []string{al.Subject}}
            }

            if al.Qstart <= currentQi.Range.End +1 +gap {
                qoIns = false
                if currentQi.Range.End < al.Qend {
                    currentQi.Range.End = al.Qend
                }
                currentQi.Seqs = append(currentQi.Seqs, al.Query)

            } else {
                qoIns = true
                qo.Regions = append(qo.Regions, currentQi)
                qo.Gaps = append(qo.Gaps, al.Qstart - currentQi.Range.End)
                qi = intv{Start: al.Qstart, End:al.Qend}
                currentQi = region{Range:qi, Seqs: []string{al.Query}}
            }
        }
        so.Regions = append(so.Regions, currentSi)
        qo.Regions = append(qo.Regions, currentQi)
        mSubQuery.NewEntry(current, so, name)
        mQuerySub.NewEntry(current, qo, name)
    }

    return mQuerySub, mSubQuery
}

func (o Overlap) GetInfo() {
    fmt.Printf("Information for overlap %v\n", o.Name)
    fmt.Printf("%v regions with overlapping sequences\n", len(o.Regions))
    for n, r := range o.Regions {
        fmt.Printf("%v: [ %v\t%v ] (%v sequences)\n", n, r.Range.Start, r.Range.End, len(r.Seqs))
    }
}

// Prints to Stdout all the indidual regions, 
// one per line, csv format with header
func (o Overlap) Csv() {
    fmt.Printf("Start,End\n")
    for _, r := range o.Regions {
        fmt.Printf("%v,%v\n", r.Range.Start, r.Range.End)
    }
}

// Prints to Stdout some statistics about our overlaps
func (o Overlap) GetStats() {
    fmt.Printf("Number of regions : %v\n", len(o.Regions))
    fmt.Printf("Min gap length : %v\n", o.minGap())
    fmt.Printf("Max gap length : %v\n", o.maxGap())
    fmt.Printf("Avg gap length : %v\n", o.avgGap())
}

func (o Overlap) minGap() int{
    var min int
    if len(o.Gaps) >= 1 {
        min = o.Gaps[0]
        for _, gap := range o.Gaps {
            if gap < min {
                min = gap
            }
        }
    }
    return min
}

func (o Overlap) maxGap() int{
    var max int
    if len(o.Gaps) >= 1 {
        max = o.Gaps[0]
        for _, gap := range o.Gaps {
            if gap > max {
                max = gap
            }
        }
    }
    return max
}

func (o Overlap) avgGap() int{
    var avg int
    switch len(o.Gaps) {
    case 0:
        return 0
    case 1:
        return o.Gaps[0]
    default:
        for _, gap := range o.Gaps {
            avg += gap
        }
        return avg / len(o.Gaps)
    }
}

