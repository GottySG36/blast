// Utility to extract and write extracted genomes into seperated files

package blast

import(
    "bufio"
    "fmt"
    "io"
    "os"
    "path"
    "regexp"
    "strings"
)

var re = regexp.MustCompile(`\s`)

func (b Blast) ReorderFna(fna, prefix string) error {

    reo := fmt.Sprintf("%v-%v", prefix, path.Base(fna))
    o, err := os.OpenFile(reo, os.O_CREATE|os.O_WRONLY, 0644)
    defer o.Close()
    if err != nil {
        return err
    }
    w := bufio.NewWriter(o)

    f, err := os.Open(fna)
    defer f.Close()
    if err != nil {
        return err
    }
    r := bufio.NewReader(f)

    var header string
    var write bool
    var bu strings.Builder
    ordered := make(map[string]string, len(b.data))
    for {
        lr, _, err := r.ReadLine()
        line := fmt.Sprintf("%s", lr)
        if err == io.EOF {
            break
        } else if len(line) == 0 {
            continue
        }

        if line[0] == '>' {
            if write {
                ordered[header] = bu.String()
                bu.Reset()
            }
            header = re.ReplaceAllLiteralString(line[1:], "")
            write = true

        } else {
            bu.WriteString(line)
        }
    }

    // Getting sequence identifiers from blast result for indexing our rows
    // Returns an error if not found, but should never happen at this point in the code. 
    iq, qok := (*b.data[0].index)["qseqid"]
    if !qok {
        err := fmt.Errorf("Missing required column in blast : %v", "qseqid")
        return err
    }

    for _, r := range b.data {
        if seq, ok := ordered[r.data[iq].(string)]; ok {
            w.WriteString(fmt.Sprintf(">%v\n", r.data[iq]))
            w.WriteString(seq)
            w.WriteString("\n")
        }
    }
    w.Flush()

    return nil
}
