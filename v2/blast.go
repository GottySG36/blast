// Utility to sort and extract the best blast match

package blast

import (
    "bufio"
    "fmt"
    "io"
    "os"
    "regexp"
    "sort"
    "strconv"
    "strings"

    "bitbucket.org/GottySG36/pangenome"
)

// Defines what types are wach individual columns in the blast table
// They will be used to call the appropriate type conversion function
var (
    stringCol = map[string]bool {
        "qseqid"      : true,
        "qgi"         : true,
        "qacc"        : true,
        "sseqid"      : true,
        "sallseqid"   : true,
        "sgi"         : true,
        "sallgi"      : true,
        "sacc"        : true,
        "sallacc"     : true,
        "qseq"        : true,
        "sseq"        : true,
        "frames"      : true,
        "btop"        : true,
        "staxids"     : true,
        "sscinames"   : true,
        "sskingdoms"   : true,
        "sstrand"     : true,
    }

    intCol = map[string]bool {
        "qlen"        : true,
        "slen"        : true,
        "qstart"      : true,
        "qend"        : true,
        "sstart"      : true,
        "send"        : true,
        "length"      : true,
        "mismatch"    : true,
        "positive"    : true,
        "gapopen"     : true,
        "gaps"        : true,
        "qframe"      : true,
        "sframe"      : true,
    }

    floatCol = map[string]bool {
        "evalue"      : true,
        "bitscore"    : true,
        "score"       : true,
        "pident"      : true,
        "nident"      : true,
        "ppos"        : true,
        "qcovs"       : true,
        "qcovhsp"     : true,
        "qcovus"      : true,
    }
)

// A row consists of a list of values. 
// The type for each individual value is determined using the map variables 
// defined above, along with the columns slice for the Blast struct (a slice of strings)
// It also contains a map tolist the correspondance between columns names and the list index
type row struct {
    index   *(map[string]int)
    data    []interface{}
}

// The main struct to describe the Blast result data table
// Consists of a list of `columns` names, and
// the actual `data` table
type Blast struct {
    columns []string
    data []row
}

func initRow(n int) row {
    var r row
    r.data = make([]interface{}, n, n)
    return r
}

// Helper function used to parse each line found in the blast results file. 
// Each line should already have split on the appropriate field delimiter ('\t' normally).
// This means that we here provide two slices of strings. The first being the actual data. 
// The other one is there to describe what each value of our data is. 
//
// Return a `row` and an error. 
func getRow(l []string, c []string) (row, error) {
    r := initRow(len(c))
    // Length of row (len(l)) must match number of columns (len(c))
    if len(l) != len(c) {
        return r, fmt.Errorf("Number of values found in data row (%v) doesn't match expected number of values (%v)", len(l), len(c))
    }

    for idx, val := range c {
        if stringCol[val] {
            r.data[idx] = l[idx]
        } else if intCol[val] {
            i, err := strconv.Atoi(l[idx])
            if err != nil{
                return r, err
            }
            r.data[idx] = i
        } else if floatCol[val] {
            f, err := strconv.ParseFloat(l[idx], 64)
            if err != nil {
                return r, err
            }
            r.data[idx] = f
        } else {
            return r, fmt.Errorf("Invalid column received as argument : %v", val)
        }
    }

    return r, nil
}

func LoadBlast(file string, c []string) (Blast, error) {

    cIndex := make(map[string]int, len(c))
    for idx, val := range c {
        cIndex[val] = idx
    }
    var b Blast
    b.columns = c
    f, err := os.Open(file)
    defer f.Close()
    if err != nil {
        return b, err
    }

    var sb strings.Builder
    r := bufio.NewReader(f)
    for {
        read, more, err := r.ReadLine()
        if err == io.EOF {
            break
        } else if len(read) == 0 {
            continue
        } else if err != nil {
            return b, err
        } else if len(read) == 0 {
            continue
        }
        sb.Write(read)
        if more {
            continue
        }

        line := strings.Split(sb.String(), "\t")
        sb.Reset()
        r_l, err := getRow(line, c)
        if err != nil {
            return b, err
        }
        r_l.index = &cIndex
        b.data = append(b.data, r_l)
    }

    return b, nil
}

func (r row) String() string {
    var sep string
    var b strings.Builder
    for _, val := range r.data {
        switch v := val.(type) {
            case int:
                b.WriteString(fmt.Sprintf("%v%v", sep, v) )
            case float64:
                b.WriteString(fmt.Sprintf("%v%v", sep, v) )
            case string:
                b.WriteString(sep)
                b.WriteString(v)
        }
        sep = ","
    }
    return b.String()
}

func (b Blast) Pretty(nb int) {
    fmt.Fprintf(os.Stdout, "%v\n", strings.Join(b.columns, ",") )
    if (nb < 0) || (nb > len(b.data)) {
        nb = len(b.data)
    }
    for i := 0; i < nb; i++ {
        fmt.Fprintf(os.Stdout, "%v\n", b.data[i])
    }
}

func (b Blast) Write(d, o string) error {
    f, err := os.Create(fmt.Sprintf("%v/%v", d, o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    for _, row := range b.data {
        fmt.Fprintf(w, "%v\n", row)
    }
    w.Flush()

    return nil
}

func (d Duplicated) PrintDup() {
    for _, dup := range d {
        for _, row := range dup {
            fmt.Printf("%v\n", row)
        }
    }
}

func (d Duplicated) Write(dir, o string) error {
    f, err := os.Create(fmt.Sprintf("%v/%v", dir, o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    for _, dup := range d {
        for _, row := range dup {
            fmt.Fprintf(w, "%v\n", row)
        }
    }
    w.Flush()

    return nil
}

type lessFunc func(p1, p2 *row) bool

// multiSorter implements the Sort interface, sorting the changes within.
type multiSorter struct {
    blast   Blast
    less    []lessFunc
}

// Sort sorts the argument slice according to the less functions passed to OrderedBy.
func (ms *multiSorter) Sort(b Blast) {
    ms.blast = b
    sort.Sort(ms)
}

// OrderedBy returns a Sorter that sorts using the less functions, in order.
// Call its Sort method to sort the data.
func OrderedBy(less []lessFunc) *multiSorter {
    return &multiSorter{
            less: less,
    }
}

// Len is part of sort.Interface.
func (ms *multiSorter) Len() int {
    return len(ms.blast.data)
}

// Swap is part of sort.Interface.
func (ms *multiSorter) Swap(i, j int) {
    ms.blast.data[i], ms.blast.data[j] = ms.blast.data[j], ms.blast.data[i]
}

// Less is part of sort.Interface. It is implemented by looping along the
// less functions until it finds a comparison that discriminates between
// the two items (one is less than the other). 
func (ms *multiSorter) Less(i, j int) bool {
    p, q := &ms.blast.data[i], &ms.blast.data[j]
    // Try all but the last comparison.
    var k int
    for k = 0; k < len(ms.less)-1; k++ {
        less := ms.less[k]
        switch {
        case less(p, q):
            // p < q, so we have a decision.
            return true
        case less(q, p):
            // p > q, so we have a decision.
            return false
        }
        // p == q; try the next comparison.
    }
    // All comparisons to here said "equal", so just return whatever
    // the final comparison reports.
    return ms.less[k](p, q)
}

// Closures that order the Change structure.
var AZqseqid = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["qseqid"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "qseqid") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a < b
}
var AZsseqid = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["sseqid"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "sseqid") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a < b
}

var DecLength = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["length"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "length") ) }
    a, _ := c1.data[i].(int)
    b, _ := c2.data[i].(int)
    return a > b
}
var IncLength = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["length"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "length") ) }
    a, _ := c1.data[i].(int)
    b, _ := c2.data[i].(int)
    return a < b
}
var DecPident = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["pident"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "pident") ) }
    a, _ := c1.data[i].(float64)
    b, _ := c2.data[i].(float64)
    return a > b
}
var DecQcovhsp = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["qcovhsp"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "qcovhsp") ) }
    a, _ := c1.data[i].(float64)
    b, _ := c2.data[i].(float64)
    return a > b
}
var IncEvalue = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["evalue"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "evalue") ) }
    a, _ := c1.data[i].(float64)
    b, _ := c2.data[i].(float64)
    return a < b
}
var IncSstart = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["sstart"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "sstart") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a < b
}
var DecSstart = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["sstart"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "sstart") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a > b
}
var IncQstart = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["qstart"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "qstart") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a < b
}
var DecQstart = func(c1, c2 *row) bool {
    i, ok := (*c1.index)["qstart"]
    if !ok {panic(fmt.Sprintf("Oups! Somehow we got a sort parameter for column %v, but no such columns in file!", "qstart") ) }
    a, _ := c1.data[i].(string)
    b, _ := c2.data[i].(string)
    return a > b
}

// Lists the different functions to sort our table. 
// We use the following naming convention : 
// names should be the same has the different column names.
// To sort values decreasingly, we capitalize the first letter, if available
var Closures = map[string]func(*row, *row) bool {
    "qseqid"  : AZqseqid,
    "sseqid"  : AZsseqid,
    "Length"  : DecLength,
    "length"  : IncLength,
    "Pident"  : DecPident,
    "Qcovhsp" : DecQcovhsp,
    "evalue"  : IncEvalue,
    "sstart"  : IncSstart,
    "Sstart"  : DecSstart,
    "qstart"  : IncQstart,
    "Qstart"  : DecQstart,
}

func ListSortClosures(s string) ([]lessFunc, error) {
    srt := strings.Split(s, ",")
    fct := make([]lessFunc, len(srt), len(srt))
    for idx, v := range srt {
        if c := Closures[v]; c == nil {
            return nil, fmt.Errorf("Invalid sort parameter provided in arguments : %v.\nSee help for a list of available sortable fields", v)
        } else {
            fct[idx] = c
        }
    }
    return fct, nil
}

type pair [2]string

func (r row) isIdentical(n row) bool {
    var cols int
    // qseqid : 1, pident : 2, qcovhsp : 4
    for col, _ := range *(r.index){
        switch col {
        case "qseqid":
            cols += 1
        case "pident":
            cols += 2
        case "qcovhsp":
            cols += 4
        }
    }

    switch cols {
    case 3: //qseqid + pident 
        q, p := (*r.index)["qseqid"], (*r.index)["pident"]
        return r.data[q] == r.data[q] && r.data[p] == n.data[p]
    case 5: //qseqid + qcovhsp 
        q, c := (*r.index)["qseqid"], (*r.index)["qcovhsp"]
        return r.data[q] == r.data[q] && r.data[c] == n.data[c]
    case 7: //qseqid + pident + qcovhsp
        q, p, c := (*r.index)["qseqid"], (*r.index)["pident"], (*r.index)["qcovhsp"]
        return r.data[q] == r.data[q] && r.data[p] == n.data[p] && r.data[c] == n.data[c]
    }
    return false
}

type match struct {
    count int
    index int
}

type identMatch map[string]*match

type Duplicated [][]*row

func (i identMatch) max() []int {
    top := make([]int, 0)
    var max int
    for _, m := range i {
        if m.count > max {
            max = m.count
        }
    }
    for _, m := range i {
        if m.count == max {
            top = append(top, m.count)
        }
    }
    return top
}


func ExtractBest(bs Blast, p float64,  fKing, fNames string) (Blast, Duplicated, error){
    found := make(map[string]bool)

    var best Blast
    best.columns = make([]string, len(bs.columns))
    copy(best.columns, bs.columns)

    cIndex := make(map[string]int, len(best.columns))
    for idx, val := range best.columns {
        cIndex[val] = idx
    }

    dup := make(Duplicated, 0)

    comma, _ := regexp.Compile(`,`)
    regKing,  err := regexp.Compile(fmt.Sprintf(`%v`, comma.ReplaceAllString(fKing,  "|")))
    if err != nil {
        return best, nil, err
    }

    regNames, err := regexp.Compile(fmt.Sprintf(`%v`, comma.ReplaceAllString(fNames, "|")))
    if err != nil {
        return best, nil, err
    }

    // Getting sequence identifiers from blast result for indexing our rows
    // Returns an error if not found, but should never happen at this point in the code. 
    iq, qok := (*bs.data[0].index)["qseqid"]
    _, sok := (*bs.data[0].index)["sseqid"]
    ip, pok := (*bs.data[0].index)["pident"]
    it, tok := (*bs.data[0].index)["staxids"]
    isk, skok := (*bs.data[0].index)["sskingdoms"]
    isn, snok := (*bs.data[0].index)["sscinames"]
    if !sok || !qok || !pok || !tok{
        var missing strings.Builder
        var sep = ""
        if !sok { missing.WriteString(sep + "sseqid" ); sep = ", " }
        if !qok { missing.WriteString(sep + "qseqid" ); sep = ", " }
        if !pok { missing.WriteString(sep + "pident" ); sep = ", " }
        if !tok { missing.WriteString(sep + "staxids"); sep = ", " }

        err = fmt.Errorf("Missing required column(s) in blast : %v", missing.String())
        return best, nil, err
    }

    for i, r := range bs.data {
        ident := make(identMatch)
        if skok {
            if regKing.MatchString(r.data[isk].(string)) { continue }
        } else if  snok {
            if regNames.MatchString(r.data[isn].(string)) { continue }
        } else if r.data[ip].(float64) < p {
            continue
        }

        if found[ r.data[iq].(string) ] != true {
            next := 1
            for {
                if i+next >= len(bs.data) {
                    break
                } else if skok {
                    if regKing.MatchString(bs.data[i+next].data[isk].(string)) { next++; continue }

                } else if snok {
                    if regNames.MatchString(bs.data[i+next].data[isn].(string)) { next++; continue }

                } else if r.isIdentical(bs.data[i+next]) {
                    if _, ok := ident[bs.data[i+next].data[it].(string)]; !ok {
                        m := &match{count:1, index:i+next}
                        ident[bs.data[i+next].data[it].(string)] = m
                    } else {
                        ident[bs.data[i+next].data[it].(string)].count++
                    }
                    next++
                    continue
                }
                break
            }
            found[r.data[iq].(string)] = true
            top := make([]int, 0)
            if len(ident) > 1 {
                id := make([]*row, 0)
                for _, ix := range ident {
                    id = append(id, &bs.data[ix.index])
                }
                dup = append(dup, id)
                top = ident.max()
            } else {
                top = []int{i}
            }
            best.data = append(best.data, bs.data[top[0]])
        }
    }
    return best, dup, nil
}

func (bs Blast) ExtractGenomes() (map[string]map[string]bool, error) {
    // Getting sequence identifiers from blast result for indexing our rows
    // Returns an error if not found, but should never happen at this point in the code. 
    iq, qok := (*bs.data[0].index)["qseqid"]
    it, tok := (*bs.data[0].index)["staxids"]
    if !qok || !tok {
        var missing strings.Builder
        var sep = ""
        if !qok {
            missing.WriteString(sep+"qseqid")
            sep = ", "
        }
        if !tok { missing.WriteString(sep+"staxids") }

        err := fmt.Errorf("Missing required column(s) in blast : %v", missing.String())
        return nil, err
    }
    found   := make(map[string]bool)
    // genomes[r.Staxids[0]][r.Qseqid] = true/false
    genomes := make(map[string]map[string]bool)
    for _, r := range bs.data {
        genomes[r.data[it].(string)] = make(map[string]bool)
    }
    for _, r := range bs.data {
        if found[r.data[iq].(string)] != true {
            found[r.data[iq].(string)] = true
            genomes[r.data[it].(string)][r.data[iq].(string)] = true
        }
    }
    return genomes, nil
}
func getTaxid(n, sep string) string {
    return strings.Split(n, sep)[2]
}

func getSampleId(n, sep string) string {
    return strings.Split(n, sep)[1]
}

var picker = map[string](func(string, string) string){"taxID":getTaxid, "sampleID":getSampleId}

func (b Blast) GetPangenome(t pangenome.Taxids, core, acc float64, k, s string) (pangenome.Pangenome, error) {
    iq, qok := (*b.data[0].index)["qseqid"]
    is, sok := (*b.data[0].index)["sseqid"]
    if !qok || !sok {
        var missing strings.Builder
        var sep = ""
        if !qok {
            missing.WriteString(sep+"qseqid")
            sep = ", "
        }
        if !sok { missing.WriteString(sep+"sseqid") }

        err := fmt.Errorf("Missing required column(s) in blast : %v", missing.String())
        return nil, err
    }

    pang := make(pangenome.Pangenome)
    for _, row := range b.data {
        if _, ok := pang[row.data[iq].(string)]; !ok {
            pang[row.data[iq].(string)] = &pangenome.Gene{Name:row.data[iq].(string), Counts:make(pangenome.Counts, len(t))}
        }
        pang[row.data[iq].(string)].Counts[t[picker[k](row.data[iq].(string), s)]]++
        // We do not want to count twice in 
        // case it maps against itself
        if row.data[iq].(string) != row.data[is].(string) {
            pang[row.data[iq].(string) ].Counts[t[picker[k](row.data[is].(string) , s)]]++
        }
        pang[row.data[iq].(string) ].Category = pang[row.data[iq].(string) ].Counts.Categorize(core, acc)
    }
    return pang, nil
}

