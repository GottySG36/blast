// Utility to sort and extract the best blast match

package blast

import (
    "bufio"
    "fmt"
    "os"
    "regexp"
    "sort"
    "strconv"
    "strings"

    "bitbucket.org/GottySG36/pangenome"
)

type row struct {
    Qseqid  string
    Sseqid  string
    Pident  float64
    Qlen    int
    Length  int
    Qcovhsp float64
    Evalue  float64
    Sstart  int
    Send    int
    Staxids []string
    Sskingdoms  string
    Sscinames   string
}
type Blast []row

func (r row) String() string {
    return fmt.Sprintf("%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v",
                        r.Qseqid, r.Sseqid, r.Pident, r.Qlen, r.Length, r.Qcovhsp,
                        r.Evalue, r.Sstart, r.Send, r.Staxids, r.Sskingdoms, r.Sscinames)
}


func (d Duplicated) PrintDup() {
    for _, dup := range d {
        for _, row := range dup {
            fmt.Printf("%v\n", row)
        }
    }
}

func (d Duplicated) Write(dir, o string) error {
    f, err := os.Create(fmt.Sprintf("%v/%v", dir, o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    for _, dup := range d {
        for _, row := range dup {
            fmt.Fprintf(w, "%v\n", row)
        }
    }
    w.Flush()

    return nil
}

func (b Blast) Write(d, o string) error {
    f, err := os.Create(fmt.Sprintf("%v/%v", d, o))
    defer f.Close()
    if err != nil {
        return err
    }

    w := bufio.NewWriter(f)

    for _, row := range b {
        fmt.Fprintf(w, "%v\n", row)
    }
    w.Flush()

    return nil
}

func (b Blast) Pretty(nb int) {
    fmt.Fprintf(os.Stdout, "%v\n","qseqid,sseqid,pident,qlen,length,qcovhsp,evalue,sstart,send,staxids,sskingdoms,sscinames")
    if (nb < 0) || (nb > len(b)) {
        nb = len(b)
    }
    for i := 0; i < nb; i++ {
        fmt.Fprintf(os.Stdout, "%v\n", b[i])
    }
}


func getRow(l []string) (row, error) {
    r := row{}
    var err error
    r.Qseqid = l[0]
    r.Sseqid = l[1]
    r.Pident, err = strconv.ParseFloat(l[2], 64)
    if err != nil {
        return r, err
    }
    r.Qlen, err = strconv.Atoi(l[3])
    if err != nil {
        return r, err
    }
    r.Length, err = strconv.Atoi(l[4])
    if err != nil {
        return r, err
    }
    r.Qcovhsp, err = strconv.ParseFloat(l[5], 64)
    if err != nil {
        return r, err
    }
    r.Evalue, err = strconv.ParseFloat(l[6], 64)
    if err != nil {
        return r, err
    }
    r.Sstart, err = strconv.Atoi(l[7])
    if err != nil {
        return r, err
    }
    r.Send, err = strconv.Atoi(l[8])
    if err != nil {
        return r, err
    }
    r.Staxids = strings.Split(l[9], ";")

    r.Sskingdoms = l[10]
    r.Sscinames = l[11]

    if r.Sstart > r.Send {
        r.Sstart, r.Send = r.Send, r.Sstart
    }

    return r, nil
}

func LoadBlast(file string) (Blast, error) {

    var b Blast

    f, err := os.Open(file)
    defer f.Close()
    if err != nil {
        return nil, err
    }

    r := bufio.NewScanner(f)
    for r.Scan() {
        line := strings.Split(r.Text(), "\t")
        r_l, err := getRow(line)
        if err != nil {
            return nil, err
        }
        b = append(b, r_l)
    }

    return b, nil
}

type lessFunc func(p1, p2 *row) bool

// multiSorter implements the Sort interface, sorting the changes within.
type multiSorter struct {
    Blast []row
    less    []lessFunc
}

// Sort sorts the argument slice according to the less functions passed to OrderedBy.
func (ms *multiSorter) Sort(Blast []row) {
    ms.Blast = Blast
    sort.Sort(ms)
}

// OrderedBy returns a Sorter that sorts using the less functions, in order.
// Call its Sort method to sort the data.
func OrderedBy(less ...lessFunc) *multiSorter {
    return &multiSorter{
            less: less,
    }
}

// Len is part of sort.Interface.
func (ms *multiSorter) Len() int {
    return len(ms.Blast)
}

// Swap is part of sort.Interface.
func (ms *multiSorter) Swap(i, j int) {
    ms.Blast[i], ms.Blast[j] = ms.Blast[j], ms.Blast[i]
}

// Less is part of sort.Interface. It is implemented by looping along the
// less functions until it finds a comparison that discriminates between
// the two items (one is less than the other). 
func (ms *multiSorter) Less(i, j int) bool {
    p, q := &ms.Blast[i], &ms.Blast[j]
    // Try all but the last comparison.
    var k int
    for k = 0; k < len(ms.less)-1; k++ {
        less := ms.less[k]
        switch {
        case less(p, q):
            // p < q, so we have a decision.
            return true
        case less(q, p):
            // p > q, so we have a decision.
            return false
        }
        // p == q; try the next comparison.
    }
    // All comparisons to here said "equal", so just return whatever
    // the final comparison reports.
    return ms.less[k](p, q)
}


// Closures that order the Change structure.
var AZqseqid = func(c1, c2 *row) bool {
    return c1.Qseqid <= c2.Qseqid
}
var AZsseqid = func(c1, c2 *row) bool {
    return c1.Sseqid <= c2.Sseqid
}

var Length = func(c1, c2 *row) bool {
    return c1.Length > c2.Length
}
var Pident = func(c1, c2 *row) bool {
    return c1.Pident > c2.Pident
}
var Qcovhsp = func(c1, c2 *row) bool {
    return c1.Qcovhsp > c2.Qcovhsp
}
var IncEvalue = func(c1, c2 *row) bool {
    return c1.Evalue < c2.Evalue
}
var IncSstart = func(c1, c2 *row) bool {
    return c1.Sstart < c2.Sstart
}

type pair [2]string

func (r row) isIdentical(n row) bool {
    return r.Qseqid == n.Qseqid && r.Pident == n.Pident && r.Qcovhsp == n.Qcovhsp
}

type match struct {
    count int
    index int
}

type identMatch map[string]*match

type Duplicated [][]*row

func (i identMatch) max() []int {
    top := make([]int, 0)
    var max int
    for _, m := range i {
        if m.count > max {
            max = m.count
        }
    }
    for _, m := range i {
        if m.count == max {
            top = append(top, m.count)
        }
    }
    return top
}

func ExtractBest(bs Blast, p float64,  fKing, fNames string) (Blast, Duplicated, error){
    found := make(map[string]bool)
    best  := make(Blast, 0)
    dup := make(Duplicated, 0)
    comma, _ := regexp.Compile(`,`)
    regKing,  err := regexp.Compile(fmt.Sprintf(`%v`, comma.ReplaceAllString(fKing,  "|")))
    if err != nil {
        return nil, nil, err
    }
    regNames, err := regexp.Compile(fmt.Sprintf(`%v`, comma.ReplaceAllString(fNames, "|")))
    if err != nil {
        return nil, nil, err
    }

    for i, r := range bs {
        ident := make(identMatch)
        if regKing.MatchString(r.Sskingdoms) || regNames.MatchString(r.Sscinames) || r.Pident < p {
            continue
        }
        if found[r.Qseqid] != true {
            next := 1
            for {
                if i+next >= len(bs) {
                    break
                } else if regKing.MatchString(bs[i+next].Sskingdoms) || regNames.MatchString(bs[i+next].Sscinames) {
                    next++
                    continue
                } else if r.isIdentical(bs[i+next]) {
                    if _, ok := ident[bs[i+next].Sscinames]; !ok {
                        m := &match{count:1, index:i+next}
                        ident[bs[i+next].Sscinames] = m
                    } else {
                        ident[bs[i+next].Sscinames].count++
                    }
                    next++
                    continue
                }
                break
            }
            found[r.Qseqid] = true
            top := make([]int, 0)
            if len(ident) > 1 {
                id := make([]*row, 0)
                for _, ix := range ident {
                    id = append(id, &bs[ix.index])
                }
                dup = append(dup, id)
                top = ident.max()
            } else {
                top = []int{i}
            }
            best = append(best, bs[top[0]])
        }
    }
    return best, dup, nil
}

func (bs Blast) ExtractGenomes() map[string]map[string]bool {
    found   := make(map[string]bool)
    // genomes[r.Staxids[0]][r.Qseqid] = true/false
    genomes := make(map[string]map[string]bool)
    for _, r := range bs {
        genomes[r.Staxids[0]] = make(map[string]bool)
    }
    for _, r := range bs {
        if found[r.Qseqid] != true {
            found[r.Qseqid] = true
            genomes[r.Staxids[0]][r.Qseqid] = true
        }
    }
    return genomes
}

func getTaxid(n, sep string) string {
    return strings.Split(n, sep)[2]
}

func getSampleId(n, sep string) string {
    return strings.Split(n, sep)[1]
}

var picker = map[string](func(string, string) string){"taxID":getTaxid, "sampleID":getSampleId}

func (b Blast) GetPangenome(t pangenome.Taxids, core, acc float64, k, s string) pangenome.Pangenome {
    pang := make(pangenome.Pangenome)
    for _, row := range b {
        if _, ok := pang[row.Qseqid]; !ok {
            pang[row.Qseqid] = &pangenome.Gene{Name:row.Qseqid, Counts:make(pangenome.Counts, len(t))}
        }
        pang[row.Qseqid].Counts[t[picker[k](row.Qseqid, s)]]++
        // We do not want to count twice in 
        // case it maps against itself
        if row.Qseqid != row.Sseqid {
            pang[row.Qseqid].Counts[t[picker[k](row.Sseqid, s)]]++
        }
        pang[row.Qseqid].Category = pang[row.Qseqid].Counts.Categorize(core, acc)
    }
    return pang
}
